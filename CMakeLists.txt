cmake_minimum_required (VERSION 3.4)

project(kdtree_multiobjs
		VERSION 1.0
		LANGUAGES CXX)

# Find Package
find_package(Eigen3 REQUIRED)
find_package(PCL 1.8 REQUIRED)
find_package(Boost COMPONENTS thread REQUIRED)

# Install Variables
include(GNUInstallDirs)

## Compile as C++14, supported in ROS Kinetic and newer
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Set variables
set(LIB_NAME kdtreemo)

# add Libraries
add_library(${LIB_NAME} SHARED src/KDtree.cpp)

# Include Directories
target_include_directories(${LIB_NAME} PUBLIC include ${EIGEN3_INCLUDE_DIRS} ${PCL_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}) 

#compile extras options
add_compile_options(-O3)#-Wall -Wextra -O3)
option(COMPILE_EXAMPLE "Compile the test function" OFF)

# Headers
set(HEADERS_FLCL
	include/KDtree.h
)

# ------------------------------------------------------- TEST CODE
option( BUILD_EXAMPLE "Set ON to build the example" OFF)

if (BUILD_EXAMPLE)
	# Executable for testing
	add_executable(${PROJECT_NAME}_example examples/example_main.cpp)

	target_include_directories(${PROJECT_NAME}_example PUBLIC include ${EIGEN3_INCLUDE_DIRS} ${PCL_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

	# Specify libraries to link a library or executable target against
	target_link_libraries(${PROJECT_NAME}_example
	 ${PCL_LIBRARIES}
	 ${LIB_NAME}
	 ${Boost_LIBRARIES}
	)
endif()
# ------------------------------------------------------- INSTALL

# Set Version properties
set_target_properties(${LIB_NAME} PROPERTIES VERSION ${PROJECT_VERSION} 
								PUBLIC_HEADER "${HEADERS_FLCL}")

# Install target
install(TARGETS ${LIB_NAME}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
