[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

[K-D tree](https://en.wikipedia.org/wiki/K-d_tree) is a structure to organise points into the k-dimentional space. It partitions the space in k layers, 
each of them accounting for only a single dimension of the space we are working with. They are an extension to 
multi-dimensions space of the binary tree.

Thanks to their structure, operations such as _find the nearest points_, _finding neighbour points_ and _points within a hyper-sphere_
are very efficient. The distance between two points is usually computed the Euclidean distance. However, we wanted to
extend the algorithm to compute multi-objective criteria. Moreover, we were interested to use this algorithm in the context
of robotic planning. Therefore, the focus of this project accounts for the robot while it is generating the tree.  

Here, we propose the KDtree-MultiObj, which accounts for the manipulability of the robot and distance between the end-effector poses
using the Euclidean distance.

## Requirements:

    apt install build-essential libeigen3-dev libpcl-dev git libboost-dev cmake -y

## Install:

    mkdir build
    cd build
    cmake .. 

If you want to build the examples use the next code instead:

    cmake -DBUILD_EXAMPLE=ON .. 

Then:

    make -j 4
    sudo make install

## Future works
We plan to extend this work to multi-objective criteria that may define outside the algorithm. In this way, the method would
allow the plug-and-play of different criteria.
