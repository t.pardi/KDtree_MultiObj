//Copyright (c) 2020, Tommaso Pardi
//All rights reserved.
//
//This source code is licensed under the BSD-style license found in the
//LICENSE file in the root directory of this source tree.

#include <stdlib.h>     /* srand, rand */
#include <KDtree.h>
#include <iostream>
#include <string>
// pcl
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <random>
#include <chrono>
#include <ctime>

int main(){
  
	KDtree T;
	Eigen::Vector3d v = {0,0,0};
	Eigen::Vector3d v_old = {0,0,0};
	Eigen::Quaterniond Q(1,0,0,0);
	Eigen::VectorXd q(6);
	q << 0,0,0,0,0,0;	
	std::vector<double> obj = {0};
	std::vector<double> e = {0};
	// Visualise in 3d
	int N = 5000;
  	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;

	// Random numbers generator
 	std::random_device rd;
 	std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dis(-10,10);
	
	std::cout << "--------Base Node" << std::endl;
	T.insert(v, Q, obj, obj, q, q, 0.0, 0.0, v);

	std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
	std::cout << "--------Add Nodes" << std::endl;

	for (int i = 0; i < 50000; ++i){
		v_old = v;
		v(0) = (int) dis(gen);
		v(1) = (int) dis(gen);
		v(2) = (int) dis(gen);
		for (int j = 0; j < obj.size(); ++j)
			obj[j] = dis(gen);

		T.insert(v, Q, obj, obj, q, q, 0.0, 0.0, v_old);
	}

	end = std::chrono::system_clock::now();

	std::cout << (double)  std::chrono::duration_cast<std::chrono::microseconds> (end-start).count() / 50000 << std::endl;

	std::cout << "--------NN" << std::endl;
	Node nearest;

	v(0) = v(0) + 0.1;

    start = std::chrono::system_clock::now();
 

    for (int i = 0; i < N; ++i){
		v(0) = (int) dis(gen);
		v(1) = (int) dis(gen);
		v(2) = (int) dis(gen);
		T.NN(v, nearest);
    }
	
	end = std::chrono::system_clock::now();
	
	std::cout << (double) std::chrono::duration_cast<std::chrono::microseconds> (end-start).count() / N << std::endl;

	std::cout << "(" <<nearest.p_(0) << ", " << nearest.p_(1)<< ", " << nearest.p_(2) << ") " << std::endl;
	
	std::cout << "--------NNeighbourhood" << std::endl;
	std::cout << "Origin point: (" <<v(0) << ", " << v(1)<< ", " << v(2) << ") " << std::endl;

	std::vector<Node> vecNode;
    start = std::chrono::system_clock::now();

	for (int i = 0; i < N; ++i)
	T.NNS(v, vecNode, 1);

	end = std::chrono::system_clock::now();

	std::cout << (double)  std::chrono::duration_cast<std::chrono::microseconds> (end-start).count() / N << std::endl;


    std::cout << "Elements in the neighourhood: " <<vecNode.size() << std::endl;

    for (int i = 0; i < vecNode.size(); ++i)
    std::cout << "(" <<vecNode[i].p_(0) << ", " << vecNode[i].p_(1)<< ", " << vecNode[i].p_(2) << ") ";

    std::cout << std::endl;
	
    viewer = T.visualiseTree(v , 0.2);
    std::cout << "Finish to compose the viewer Tree" << std::endl;

    //--------------------
    // -----Main loop-----
    //--------------------
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }

	return 0;
  	
}
