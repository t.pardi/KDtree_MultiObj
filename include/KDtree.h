//Copyright (c) 2020, Tommaso Pardi
//All rights reserved.
//
//This source code is licensed under the BSD-style license found in the
//LICENSE file in the root directory of this source tree.

// Eigen
#include <Eigen/Dense>
// Fstream
#include <fstream>
#include <iostream> 
// PCL libraries
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>

#ifndef KDTREE_M_HEADER_COMMON
#define KDTREE_M_HEADER_COMMON

#define SPACE_COUNT 10
#define LEFT_SIDE 0
#define RIGHT_SIDE 1

class Node{
public:
	
	// Constructor
	Node(){
		left_tree_ = NULL;
		right_tree_ = NULL;
		parent_ = NULL;
	}
	// Copy Constructor
	Node(const Node& n){

		*this = n;

		left_tree_ = n.left_tree_;
		right_tree_ = n.right_tree_;
	}

	Node(const int id, const Eigen::Vector3d p, const Eigen::Quaterniond Q, const std::vector<double> obj, const std::vector<double> objIdx, const Eigen::VectorXd q, const Eigen::VectorXd dq, const double dist, const double cost, const int depth, Node* parent = NULL){
		id_ = id;
		p_ = p;
		Q_ = Q;
		obj_ = obj;
		objIdx_ = objIdx;
		q_ = q;
		dq_ = dq;
		dist_ = dist;
		cost_ = cost;
		depth_ = depth;
		left_tree_ = NULL;
		right_tree_ = NULL;
		parent_ = parent;
	}

 	void operator =(Node const &n) { 

		id_ = n.id_;
		p_ = n.p_;
		Q_ = n.Q_;
		obj_ = n.obj_;
		objIdx_ = n.objIdx_;
		q_ = n.q_;
		dq_ = n.dq_;
		dist_ = n.dist_;
		cost_ = n.cost_;
		depth_ = n.depth_;
		left_tree_ = NULL;
		right_tree_ = NULL;
		parent_ = n.parent_;
    } 
 	 
	// Denstructor
	~Node(){
		// delete test_;
		if (left_tree_ != NULL){
			delete left_tree_;
			left_tree_ = NULL;
		}
		if (right_tree_ != NULL){
			delete right_tree_;
			right_tree_ = NULL;
		}
		parent_ = NULL;

	}

	// Node ID
	int id_;
	// Point into Node 
	Eigen::Vector3d p_;
	// Orientation
	Eigen::Quaternion<double, Eigen::DontAlign> Q_;
	// Manipulability integral to this point
	std::vector<double> obj_;
	// Manipulability Index for this point
	std::vector<double> objIdx_;
	// Configuration of the robot
	Eigen::VectorXd q_;
	// Velocity of the robot configuration
	Eigen::VectorXd dq_;
	// Distance from the origin
	double dist_;
	// Objective cost
	double cost_;
	// Depth of the Node
	int depth_;
	// Left tree pointer
	Node* left_tree_;
	// Right tree pointer
	Node* right_tree_;
	// Parent pointer
	Node* parent_;
};


class KDtree{

public:
	// Constructor
	KDtree();
	// Destructor
	~KDtree();
 
	// Insert a new element in the tree, Vector3d as input
	bool insert(const Eigen::Vector3d, const Eigen::Quaterniond, const std::vector<double>, const std::vector<double>, const Eigen::VectorXd, const Eigen::VectorXd, const double, const double, const Eigen::Vector3d);
	// Remove a new element from the tree, Vector3d as input TODO
	// bool remove(const Eigen::Vector3d);
	// Look for elements in the neighborhood, Vector3d as input, Nearest Neighborhood Search
	bool NNS(const Eigen::Vector3d, std::vector<Node>&, const double ray_NH = 0.2);
	// Look for the nearest element, Vector3d as input, Nearest Neighbor
	bool NN(const Eigen::Vector3d, Node&);
	// Check if the tree is empty
	bool inline isempty() const{
		return (tree_==NULL)?true:false;
	}
	// Print all the tree
	void print() const;
	// Print costs of all Nodes of the tree
	bool printCostVector(const int& idx = -1) const;
	// Export the tree in a csv file
	void exportCSV(std::string);
	// Get the best path for a specific goal point
	void getBestPathForVisualisation(const Eigen::Vector3d&, const double&, std::vector<Eigen::Vector3d>&, const bool verbose = false);
	// Get the best path configurations for a specific goal point
	void getBestPathConfigurationsForVisualisation(const Eigen::Vector3d&, const double&, std::vector<Eigen::VectorXd>&, const bool verbose = false);
	// Get all the lines for visualising the KDtree
	void getTreeForVisualisation(std::vector<Eigen::Vector3d>&, std::vector<Eigen::Vector3d>&);
	// Visualise the 3d Tree in pcl
	boost::shared_ptr<pcl::visualization::PCLVisualizer> visualiseTree(const Eigen::Vector3d p_goal = {0, 0, 0}, const double ray_R = -1, const bool verbose = false);
	// Visualise the 3d Tree in pcl - viewer as input
	boost::shared_ptr<pcl::visualization::PCLVisualizer> visualiseTree(boost::shared_ptr<pcl::visualization::PCLVisualizer>, const Eigen::Vector3d p_goal = {0, 0, 0}, const double ray_R = -1, const bool verbose = false);
	// Visualise the best path close to a point with a defined ray
	boost::shared_ptr<pcl::visualization::PCLVisualizer> visualiseBestPath(const Eigen::Vector3d p_goal = {0, 0, 0}, const double ray_R = -1, const bool verbose = false);
	// Visualise the best path close to a point with a defined ray
	boost::shared_ptr<pcl::visualization::PCLVisualizer> visualiseBestPath(boost::shared_ptr<pcl::visualization::PCLVisualizer>, const Eigen::Vector3d, const double, const bool verbose = false);
	// Get the map generated about manipulability
	bool costMap(std::vector<Eigen::VectorXd>&, const int& idx = -1);
	// Get the trajectory generated
	bool getPath(const Eigen::Vector3d, const double, std::vector<Eigen::VectorXd>&, std::vector<Eigen::VectorXd>&, double&, std::vector<double>&, double&, const bool verbose = false);
	// Update value of a Node
	void updateNode(const Eigen::Vector3d, const double, std::vector<double>, const double, const Eigen::Vector3d);


private:
	// Recursive function for printing all Nodes
	void print(const Node*, int) const;
	// Function for recursively search the tree
	void NNS_searchTree(const Node*, const Eigen::Vector3d, std::vector<Node>&, const double, int);
	// Function for recursively search the nearest element in the tree
	void NN_searchTree(const Node*, const Eigen::Vector3d, Node&, double&, int);
	// Find the pointer of a Node
	Node* findNodePtr(const Eigen::Vector3d);
	// Export the tree in a csv file, Recursive function
	void exportCSV(const Node*, std::ofstream&);
	// Browse the tree and store all the connections
	void storeLines(const Node*, std::vector<Eigen::Vector3d>&, std::vector<Eigen::Vector3d>&);
	// Print manipubility in all Node of the tree
	void printCostVector(const Node*, const int&) const;
	// Get the map generated about manipulability recursively
	void costMap(const Node*, std::vector<Eigen::VectorXd>&, const int&);

	// Head of the tree
	Node* tree_;
	// Count element in the tree
	int ID_count_{0};

};

#endif // KDTREE_M_HEADER_COMMON