//Copyright (c) 2020, Tommaso Pardi
//All rights reserved.
//
//This source code is licensed under the BSD-style license found in the
//LICENSE file in the root directory of this source tree.

#include <KDtree.h>

KDtree::KDtree(){
	// Set pointer of the tree at NULL
	tree_ = NULL;
}

KDtree::~KDtree(){

	if (tree_ == NULL)
		return;

	delete tree_;
	tree_ = NULL;
}

bool KDtree::insert(const Eigen::Vector3d v, const Eigen::Quaterniond Q, const std::vector<double> obj, const std::vector<double> objIdx, const Eigen::VectorXd q, const Eigen::VectorXd dq, const double d, const double cost, const Eigen::Vector3d v_parent){

	//  Check if the first element is the tree is empty
	if (tree_ == NULL){
		/*Depth = 1*/
		tree_ = new Node(ID_count_++, v, Q, obj, objIdx, q, dq, d, cost, 1);
		return true;
	}

	Node* n = tree_;
	Node* n_next = tree_;

	/*
		Three layer available:
		- x -> layer = 0
		- y -> layer = 1
		- z -> layer = 2
	*/

	int layer = 0; 

	// Side
	bool side;

	// Look for the a leaf of the tree
	while (n_next != NULL){
		// Update node
		n = n_next;

		if (v(0) == n->p_(0) && v(1) == n->p_(1) && v(2) == n->p_(2))
			return false;

		if (v(layer) >= n->p_(layer)){
			// If new point is bigger than current go right
			n_next = n->right_tree_;
			side = RIGHT_SIDE;
		}
		else{
			// If new point is lower than current go left
			n_next = n->left_tree_;
			side = LEFT_SIDE;
		}

		// Increase layer of the tree and keep layer inside [0,3)
		layer = (layer + 1)%3;

	}

	Node* parent = findNodePtr(v_parent);

	if (side == LEFT_SIDE)
		// Created a new node for the tree
		n->left_tree_ = new Node(ID_count_++, v, Q, obj, objIdx, q, dq, d, cost, parent->depth_ + 1, parent);
	else // RIGHT_SIDE
		// Created a new node for the tree
		n->right_tree_ = new Node(ID_count_++, v, Q, obj, objIdx, q, dq, d, cost, parent->depth_ + 1, parent);
	

	return true;

}

void KDtree::print(const Node* root, int space) const{

	//	Root of the tree
	if (root == NULL){
		return;
	}
	
	// Increase distance between lines
	space += SPACE_COUNT;

	// Process right child first
	print(root->right_tree_, space);
	

	// Print current node after space
	std::cout << std::endl;
	for (int i = SPACE_COUNT; i < space; ++i)
		std::cout << " ";
	std::cout << "(" << root->p_(0)<<", "<< root->p_(1)<<", "<< root->p_(2) << ")";
	if (root->parent_ != NULL)
		std::cout <<  "[" << root->parent_->p_(0)<<", "<< root->parent_->p_(1)<<", "<< root->parent_->p_(2) << "]" << std::endl;
	else
		std::cout << "[NULL]" << std::endl;

	// Process left child
	print(root->left_tree_, space);
}

void KDtree::print() const{

	print(tree_, 0);
}

void KDtree::printCostVector(const Node* root, const int& idx) const {

	//	Root of the tree
	if (root == NULL)
		return;
	
	if (idx == -1)
		for (int i = 0; i < root->objIdx_.size(); ++i)
			std::cout << root->objIdx_[i] << ", ";
	else
		std::cout << root->objIdx_[idx];

	std::cout << std::endl;

	// Process right child first
	printCostVector(root->right_tree_, idx);
	// Process left child
	printCostVector(root->left_tree_, idx);

}

bool KDtree::printCostVector(const int& idx) const{

	if (idx >= tree_->obj_.size() || idx < 0)
		return false;

	printCostVector(tree_, idx);

	return true;
}


void KDtree::NNS_searchTree(const Node* tree, const Eigen::Vector3d v, std::vector<Node>& neighborhood, const double ray_NH, int layer){

	// If it is NULL tree, then return
	if (tree == NULL)
		return;

	double dist = sqrt( pow(v(0) - tree->p_(0), 2) + pow(v(1) - tree->p_(1), 2) + pow(v(2) - tree->p_(2), 2));

	// Add the element if it is within the range
	if (dist <= ray_NH){
		// Copy Node
		Node *tmp = new Node();
		*tmp = *tree;

		neighborhood.push_back(*tmp);
	}

	double dist_layer = fabs(v(layer) - tree->p_(layer));
	if (dist_layer <= ray_NH){
		NNS_searchTree(tree->left_tree_, v, neighborhood, ray_NH, (layer + 1)%3);
		NNS_searchTree(tree->right_tree_, v, neighborhood, ray_NH, (layer + 1)%3);
	}
	else{
		if (v(layer) >= tree->p_(layer)){
			// std::cout << "Right Tree " << v(layer) << " -> " << tree->p_(layer) << std::endl;
			NNS_searchTree(tree->right_tree_, v, neighborhood, ray_NH, (layer + 1)%3);
		}
		else{
			// std::cout << "Left Tree " << v(layer) << " -> " << tree->p_(layer) << std::endl;
			NNS_searchTree(tree->left_tree_, v, neighborhood, ray_NH, (layer + 1)%3);
		}
	}
}

bool KDtree::NNS(const Eigen::Vector3d v, std::vector<Node>& neighborhood, const double ray_NH){

	// The tree is empty
	if (tree_ == NULL)
		return false;

	NNS_searchTree(tree_, v, neighborhood, ray_NH, 0);

	return true;
}

bool KDtree::NN(const Eigen::Vector3d v, Node& nearest){

	// The tree is empty
	if (tree_ == NULL)
		return false;

	double d_best = 1e10;
	NN_searchTree(tree_, v, nearest, d_best, 0);

	return true;
}

void KDtree::NN_searchTree(const Node* node, const Eigen::Vector3d v, Node& nearest, double& d_best, int layer){

	if (node == NULL)
		return;

	double d = sqrt( pow(v(0) - node->p_(0), 2) + pow(v(1) - node->p_(1), 2) + pow(v(2) - node->p_(2), 2));

	if (d < d_best){
		nearest = *node;		
		d_best = d;
	}

	if (d == fabs(v(layer) - node->p_(layer))){
		if (v(layer) >= node->p_(layer)){
			// std::cout << "Right Tree " << v(layer) << " -> " << node->p_(layer) << std::endl;
			NN_searchTree(node->right_tree_, v, nearest, d_best, (layer+1)%3);
		}
		else{
			// std::cout << "Left Tree " << v(layer) << " -> " << node->p_(layer) << std::endl;
			NN_searchTree(node->left_tree_, v, nearest, d_best, (layer+1)%3);
		}
	}
	else{
		NN_searchTree(node->left_tree_, v, nearest, d_best, (layer+1)%3);
		NN_searchTree(node->right_tree_, v, nearest, d_best, (layer+1)%3);
	}
}


Node* KDtree::findNodePtr(const Eigen::Vector3d v){

	// The tree is empty
	if (tree_ == NULL)
		return NULL;

	Node* n = tree_;
	Node* best_node = tree_;

	/*
		Three layer available:
		- x -> layer = 0
		- y -> layer = 1
		- z -> layer = 2
	*/
	int layer = 0;

	// Look for the a leaf of the tree
	while (n != NULL && !(v(0) == n->p_(0) && v(1) == n->p_(1) && v(2) == n->p_(2))){


		if (v(layer) >= n->p_(layer))
			n = n->right_tree_;
		else
			n = n->left_tree_;

		// Increase layer of the tree and keep layer inside [0,3)
		layer = (layer + 1) % 3;

	}

	if (n == NULL){
		std::cout << "No parent, but the element is not the root" << std::endl;
		return NULL;
	}

	return n;
}

void KDtree::updateNode(const Eigen::Vector3d v, const double d, const std::vector<double> obj, const double cost, const Eigen::Vector3d parent){

	// Search node to update
	Node* nodeToUpdatePtr = findNodePtr(v);
	// Find Parent to attach
	Node* nodeParentPtr = findNodePtr(parent);

	nodeToUpdatePtr->obj_ = obj;
	nodeToUpdatePtr->dist_ = d;
	nodeToUpdatePtr->cost_ = cost;
	nodeToUpdatePtr->depth_ = nodeParentPtr->depth_ + 1;
	nodeToUpdatePtr->parent_ = nodeParentPtr;
}

void KDtree::exportCSV(std::string path){

	std::ofstream file;
	file.open (path.c_str());

	exportCSV(tree_, file);

	file.close();
}

void KDtree::exportCSV(const Node* node, std::ofstream& file){

	//	Node is null
	if (node == NULL){
		return;
	}
	
	// Print current node after space
	if (node->parent_ == NULL){
		file << node->id_ << "," << node->p_(0)<<", "<< node->p_(1)<<", "<< node->p_(2) << ",";
		for(int i = 0; i < node->obj_.size(); ++i)
			file << node->obj_[i] << ",";
		file <<"-1" << std::endl;
	}
	else{
		file << node->id_ << "," << node->p_(0)<<", "<< node->p_(1)<<", "<< node->p_(2) << ",";
		for(int i = 0; i < node->obj_.size(); ++i)
			file << node->obj_[i] << ",";
		file << node->parent_->id_ << std::endl;
	}

	// Process right child 
	exportCSV(node->right_tree_, file);
	// Process left child
	exportCSV(node->left_tree_, file);
}

void KDtree::getTreeForVisualisation(std::vector<Eigen::Vector3d>& p0, std::vector<Eigen::Vector3d>& p1){

	//	Node is null
	if (tree_ == NULL)
		std::cout << "[ERROR] The tree is empty" << std::endl;

	/*
		Add points
	*/

	storeLines(tree_, p0, p1);

}

void KDtree::getBestPathForVisualisation(const Eigen::Vector3d& p_goal, const double& ray_R, std::vector<Eigen::Vector3d>& best_path, const bool verbose){

	//	Node is null
	if (tree_ == NULL)
		std::cout << "[ERROR] The tree is empty" << std::endl;

	std::vector<Node> neighborhood;

	// Get neighborhood of p_goal
	NNS(p_goal, neighborhood, ray_R);

	if (neighborhood.empty()){
		if (verbose)
			std::cout << "No element within the circle at ray " << ray_R << " of (" << p_goal(0) << ", " << p_goal(1) << ", " << p_goal(2) << ")" << std::endl;
	}
	else{

		// Get element at minimum energy

		int idx_m;
		double e_m = 1e30;

		for (int i = 0; i < neighborhood.size(); ++i){
			if (e_m > neighborhood[i].cost_){
				e_m = neighborhood[i].cost_;
				idx_m = i;
			}
		}

		// Clear vector
		best_path.clear();
		// Store first element
		best_path.push_back(neighborhood[idx_m].p_);

		Node* n = findNodePtr(neighborhood[idx_m].p_);

		while (n->parent_ != NULL){
			n = n->parent_;
			best_path.push_back(n->p_);
		}		
	}
}


void KDtree::getBestPathConfigurationsForVisualisation(const Eigen::Vector3d& p_goal, const double& ray_R, std::vector<Eigen::VectorXd>& best_path, const bool verbose){

	//	Node is null
	if (tree_ == NULL)
		std::cout << "[ERROR] The tree is empty" << std::endl;

	std::vector<Node> neighborhood;

	// Get neighborhood of p_goal
	NNS(p_goal, neighborhood, ray_R);

	if (neighborhood.empty()){
		if (verbose)
			std::cout << "No element within the circle at ray " << ray_R << " of (" << p_goal(0) << ", " << p_goal(1) << ", " << p_goal(2) << ")" << std::endl;
	}
	else{

		// Get element at minimum energy

		int idx_m;
		double e_m = 1e30;

		for (int i = 0; i < neighborhood.size(); ++i){
			if (e_m > neighborhood[i].cost_){
				e_m = neighborhood[i].cost_;
				idx_m = i;
			}
		}

		// Clear vector
		best_path.clear();
		// Store first element
		best_path.push_back(neighborhood[idx_m].q_);

		Node* n = findNodePtr(neighborhood[idx_m].p_);

		while (n->parent_ != NULL){
			n = n->parent_;
			best_path.push_back(n->q_);
		}		
	}
}


boost::shared_ptr<pcl::visualization::PCLVisualizer> KDtree::visualiseTree(boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer, const Eigen::Vector3d p_goal, const double ray_R, const bool verbose){

	// --------------------------------------------
	// -----Open 3D viewer and add point cloud-----
	// --------------------------------------------
	viewer->setBackgroundColor (.8,.8,.8);
	viewer->addCoordinateSystem (0.5);
	viewer->initCameraParameters ();
	viewer->setCameraPosition(-1.05428, -0.965422, 1.03604, 0.461031, 0.242206, 0.85369);

	//	Node is null
	if (tree_ == NULL){
		std::cout << "The tree is empty" << std::endl;
		return viewer;
	}
	
	/*
		Add points
	*/
	std::vector<Eigen::Vector3d> p0, p1;

	storeLines(tree_, p0, p1);


	pcl::PointXYZRGB pcl_p0, pcl_p1;

 	std::cout << p0.size() << std::endl;

	for (int i = 0; i < p0.size(); ++i){

		std::string s = 's' + std::to_string(i);
		std::string l = 'l' + std::to_string(i);

		// First Point
		pcl_p0.x = p0[i](0);
		pcl_p0.y = p0[i](1);
		pcl_p0.z = p0[i](2);
		// Second Point
		pcl_p1.x = p1[i](0);
		pcl_p1.y = p1[i](1);
		pcl_p1.z = p1[i](2);

		// viewer->addSphere (pcl_p0, 0.01, 0.847058824, 0.694117647, 0.152941176, s.c_str());
		viewer->addLine<pcl::PointXYZRGB> (pcl_p0, pcl_p1, 0.368627451, 0.164705882, 0.749019608, l.c_str());
		viewer->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 3, l.c_str());
	}

	if (ray_R >= 0)
		visualiseBestPath(viewer, p_goal, ray_R);

	return (viewer);
}

boost::shared_ptr<pcl::visualization::PCLVisualizer> KDtree::visualiseTree(const Eigen::Vector3d p_goal, const double ray_R, const bool verbose){

	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));

	return visualiseTree(viewer, p_goal, ray_R, verbose);
}

void KDtree::storeLines(const Node* node, std::vector<Eigen::Vector3d>& p0, std::vector<Eigen::Vector3d>& p1){
	
	//	Node is null
	if (node == NULL){
		return;
	}

	// Store the line
	if (node->parent_ != NULL){
		p0.push_back(node->p_);
		p1.push_back(node->parent_->p_);

	}

	// Process right child 
	storeLines(node->right_tree_, p0, p1);
	// Process left child
	storeLines(node->left_tree_, p0, p1);
}

boost::shared_ptr<pcl::visualization::PCLVisualizer> KDtree::visualiseBestPath(const Eigen::Vector3d p_goal, const double ray_R, bool verbose){

	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));

	return visualiseBestPath(viewer, p_goal, ray_R, verbose);
}

boost::shared_ptr<pcl::visualization::PCLVisualizer> KDtree::visualiseBestPath(boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer, const Eigen::Vector3d p_goal, const double ray_R, const bool verbose){

	// --------------------------------------------
	// -----Open 3D viewer and add point cloud-----
	// --------------------------------------------
	viewer->setBackgroundColor (.8,.8,.8);
	viewer->addCoordinateSystem (0.5);
	viewer->initCameraParameters ();
	viewer->setCameraPosition(0.291683, 0.735859, -2.71408,0.00658075, 0.980813, 0.194842);
	//	Node is null
	if (tree_ == NULL)
		return viewer;

	std::vector<Eigen::Vector3d> best_path;
	std::vector<Node> neighborhood;
	pcl::PointXYZRGB pcl_p0, pcl_p1;

	// Get neighborhood of p_goal
	NNS(p_goal, neighborhood, ray_R);

	if (neighborhood.empty()){
		if (verbose)
			std::cout << "No element within the circle at ray " << ray_R << " of (" << p_goal(0) << ", " << p_goal(1) << ", " << p_goal(2) << ")" << std::endl;
		return viewer;
	}

	double l_width = 5;
	double rgb[3] = {0, 1, 0};

	// Get element at minimum energy

	int idx_m;
	double e_m = 1e30;

	for (int i = 0; i < neighborhood.size(); ++i){
		if (e_m > neighborhood[i].cost_){
			e_m = neighborhood[i].cost_;
			idx_m = i;
		}
	}

	// Clear vector
	best_path.clear();
	// Store first element
	best_path.push_back(neighborhood[idx_m].p_);

	Node* n = findNodePtr(neighborhood[idx_m].p_);

	while (n->parent_ != NULL){
		n = n->parent_;
		best_path.push_back(n->p_);
	}

	// Plot Best path

	for (int k = 0; k < ( best_path.size() - 1); ++k){
		std::string s = "sBest_" + std::to_string(k);
		std::string l = "lBest_" + std::to_string(k);
		// First Point
		pcl_p0.x = best_path[k](0);
		pcl_p0.y = best_path[k](1);
		pcl_p0.z = best_path[k](2);
		// Second Point
		pcl_p1.x = best_path[k + 1](0);
		pcl_p1.y = best_path[k + 1](1);
		pcl_p1.z = best_path[k + 1](2);

		viewer->addSphere (pcl_p0, 0.01, 0.847058824, 0.694117647, 0.152941176, s.c_str());

		viewer->addLine<pcl::PointXYZRGB> (pcl_p0, pcl_p1, rgb[0], rgb[1], rgb[2], l.c_str());
		viewer->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, l_width, l.c_str());
	}
	if (verbose)
		std::cout << "The best path found as a total value of: " << e_m << std::endl;

	return (viewer);		
}


bool KDtree::costMap(std::vector<Eigen::VectorXd>& map, const int &idx){

	if (tree_ == NULL)
		return false;

	costMap(tree_, map, idx);

	return true;
}

void KDtree::costMap(const Node* node, std::vector<Eigen::VectorXd>& map, const int &idx){

	if (node == NULL)
		return;

	Eigen::VectorXd element;
	
	int size_element = 4 + node->dq_.size();
	size_element += (idx == -1)?node->objIdx_.size():1;
	
	element.resize(size_element);

	element.head(3) = node->p_;

	element(3) = node->dist_;

	element.segment(4, node->dq_.size()) = node->dq_;

	if (idx != -1)
		element(4 + node->dq_.size()) = node->objIdx_[idx];
	else
		for (int i = 0; i < node->objIdx_.size(); ++i)
			element(4 + node->dq_.size() + i) = node->objIdx_[i];
	
	map.push_back(element);

	costMap(node->left_tree_, map, idx);
	costMap(node->right_tree_, map, idx);

}

bool KDtree::getPath(const Eigen::Vector3d p, const double ray_R, std::vector<Eigen::VectorXd>& jtrajectory, std::vector<Eigen::VectorXd>& ctrajectory, double& d_path, std::vector<double>& obj_path, double& e_path, const bool verbose){

	if (tree_ == NULL)
		return false;

	std::vector<Node> neighborhood;
	NNS(p, neighborhood, ray_R);

	if (neighborhood.empty()){
		if (verbose)
			std::cout << "No element within the circle at ray " << ray_R << " of (" << p(0) << ", " << p(1) << ", " << p(2) << ")" << std::endl;
		return false;
	}

	int idx_best = -1;
	double e_best = 1e30;

	for (int i = 0; i < neighborhood.size(); ++i){
		if ( neighborhood[i].cost_ < e_best){
			e_best = neighborhood[i].cost_;
			idx_best = i;
		}
	}
	
	Node* pnodePtr = findNodePtr(neighborhood[idx_best].p_);
	
	// Joint trajectory
	jtrajectory.push_back(pnodePtr->q_);
	// Cartesian trajectory
	ctrajectory.push_back(pnodePtr->p_);


	while (pnodePtr->parent_ != NULL){
		pnodePtr = pnodePtr->parent_;
		jtrajectory.push_back(pnodePtr->q_);
		ctrajectory.push_back(pnodePtr->p_);
	}

	// Store data for returning the best values
	e_path = e_best;

	obj_path = neighborhood[idx_best].obj_;

	// Scale obj[0]
	obj_path[0] /= neighborhood[0].depth_;
	
	d_path = neighborhood[idx_best].dist_;

	return true;
}
